package nsu.lab;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public final class Parser {

    private LinkedList<String> freeArguments;
    private final HashMap<Character, ProgramOption> shortToOption;
    private final HashMap<String, ProgramOption> longToOption;

    public Parser(@NotNull ProgramOption... options) {
        this.freeArguments = new LinkedList<>();
        this.shortToOption = new HashMap<>();
        this.longToOption = new HashMap<>();

        for (ProgramOption option : options) {
            this.shortToOption.put(option.getShortOption(), option);
            this.longToOption.put(option.getLongOption(), option);
        }
    }

    public void parse(String[] args) throws IllegalArgumentException, NoSuchElementException {
        for (Iterator<String> it = Arrays.asList(args).iterator(); it.hasNext(); ) {
            String currentArgument = it.next();
            ProgramOption currentOption;

            TYPE type = resolve(currentArgument);

            switch (type) {
                case SHORT_OPTION:
                    try {
                        currentOption = this.getOption(currentArgument.charAt(1));
                        this.processOption(it, currentOption);
                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
                case LONG_OPTION:
                    try {
                        currentOption = this.getOption(currentArgument.substring(2));
                        this.processOption(it, currentOption);
                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
                case ARGUMENT:
                    this.freeArguments.add(currentArgument);
                    break;
                case SHORT_OPTION_AND_ARGUMENT:
                    try {
                        currentOption = this.getOption(currentArgument.charAt(1));
                        int startIndex = currentArgument.indexOf('=');
                        startIndex = startIndex == -1 ? 2 : startIndex + 1;
                        currentArgument = currentArgument.substring(startIndex);

                        if (currentArgument.isEmpty()) {
                            throw new IllegalArgumentException(
                                    String.format(
                                            "Option %s is missing a required argument",
                                            currentOption.getShortOption()
                                    )
                            );
                        } else {
                            try {
                                currentOption.parseArgument(currentArgument);
                            } catch (IllegalArgumentException e) {
                                throw new IllegalArgumentException(
                                        String.format(
                                                "Failed to convert argument %s of " +
                                                        "option -%s(--%s) to proper format: %s",
                                                currentArgument,
                                                currentOption.getShortOption(),
                                                currentOption.getLongOption(),
                                                e.getMessage()
                                        )
                                );
                            }
                        }
                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
                case LONG_OPTION_AND_ARGUMENT:
                    try {
                        int index = currentArgument.indexOf('=');
                        currentOption = this.getOption(currentArgument.substring(2, index));
                        currentArgument = currentArgument.substring(index + 1);

                        if (currentArgument.isEmpty()) {
                            throw new IllegalArgumentException(
                                    String.format(
                                            "Option %s is missing a required argument",
                                            currentOption.getShortOption()
                                    )
                            );
                        } else {
                            try {
                                currentOption.parseArgument(currentArgument);
                            } catch (IllegalArgumentException e) {
                                throw new IllegalArgumentException(
                                        String.format(
                                                "Failed to convert argument %s of " +
                                                        "option -%s(--%s) to proper format: %s",
                                                currentArgument,
                                                currentOption.getShortOption(),
                                                currentOption.getLongOption(),
                                                e.getMessage()
                                        )
                                );
                            }
                        }
                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
            }
        }

    }

    @NotNull public ProgramOption getOption(char shortOption) throws NoSuchElementException {
        ProgramOption option = this.shortToOption.get(shortOption);

        if (option == null) {
            throw new NoSuchElementException(String.format("No such option: -%c", shortOption));
        }

        return option;
    }

    @NotNull public ProgramOption getOption(String longOption) throws NoSuchElementException {
        ProgramOption option = this.longToOption.get(longOption);

        if (option == null) {
            throw new NoSuchElementException(String.format("No such option: --%s", longOption));
        }

        return option;
    }

    public LinkedList<String> getFreeArguments() {
        return this.freeArguments;
    }

    private void processOption(Iterator<String> it, @NotNull ProgramOption currentOption)
            throws IllegalArgumentException {
        TYPE type;
        String currentArgument;

        if (currentOption.hasArgument()) {
            if (it.hasNext()) {
                currentArgument = it.next();
                type = resolve(currentArgument);

                if (type != TYPE.ARGUMENT) {
                    throw new IllegalArgumentException(
                            String.format(
                                    "Option %s is missing a required argument",
                                    currentOption.getShortOption()
                            )
                    );
                } else {
                    try {
                        currentOption.parseArgument(currentArgument);
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException(
                                String.format(
                                        "Failed to convert argument %s of " +
                                                "option -%s(--%s) to proper format: %s",
                                        currentArgument,
                                        currentOption.getShortOption(),
                                        currentOption.getLongOption(),
                                        e.getMessage()
                                )
                        );
                    }
                }
            } else {
                throw new IllegalArgumentException(
                        String.format(
                                "Option %s is missing a required argument",
                                currentOption.getShortOption()
                        )
                );
            }
        }
    }

    private enum TYPE {
        SHORT_OPTION,
        LONG_OPTION,
        ARGUMENT,
        SHORT_OPTION_AND_ARGUMENT,
        LONG_OPTION_AND_ARGUMENT
    }

    private TYPE resolve(String arg) throws NullPointerException {
        if (arg == null) {
            throw new NullPointerException();
        }

        if (arg.length() > 1) {
            if (arg.charAt(0) == '-') {
                if (arg.charAt(1) == '-' && arg.length() > 2) {
                    return arg.indexOf('=') != -1 ? TYPE.LONG_OPTION_AND_ARGUMENT : TYPE.LONG_OPTION;
                } else if (arg.charAt(1) != '-') {
                    // fix for -n=5 -m5 -k 0
                    // fix for -n=
                    return arg.length() > 2 ? TYPE.SHORT_OPTION_AND_ARGUMENT : TYPE.SHORT_OPTION;
                } else {
                    // fix for -n=
                    return TYPE.ARGUMENT;
                }
            } else {
                return TYPE.ARGUMENT;
            }
        } else {
            return TYPE.ARGUMENT;
        }
    }

    public static abstract class ProgramOption {
        private final char shortOption;
        private final String longOption;
        private boolean found;

        public ProgramOption(char shortOption, String longOption) {
            this.shortOption = shortOption;
            this.longOption = longOption;
            this.found = false;
        }

        public boolean isFound() {
            return this.found;
        }

        public abstract Object getArgument();

        char getShortOption() {
            return this.shortOption;
        }

        String getLongOption() {
            return this.longOption;
        }

        void markAsFound() {
            this.found = true;
        }

        abstract void parseArgument(String stringRepresentation);

        abstract boolean hasArgument();
    }

    public static class StringArgument extends ProgramOption {

        private String argument;

        public StringArgument(char shortOption, String longOption, String defaultArgument) {
            super(shortOption, longOption);
            this.argument = defaultArgument;
        }

        @Override
        public String getArgument() {
            return this.argument;
        }

        @Override
        void parseArgument(String stringRepresentation) {
            this.argument = stringRepresentation;
        }

        @Override
        boolean hasArgument() {
            return true;
        }
    }

    public static class IntegerArgument extends ProgramOption {

        private Integer argument;

        public IntegerArgument(char shortOption, String longOption, Integer defaultArgument) {
            super(shortOption, longOption);
            this.argument = defaultArgument;
        }

        @Override
        public Integer getArgument() {
            return this.argument;
        }

        @Override
        void parseArgument(String stringRepresentation) throws NumberFormatException {
            this.argument = Integer.valueOf(stringRepresentation);
        }

        @Override
        boolean hasArgument() {
            return true;
        }
    }

    public static class Flag extends ProgramOption {

        public Flag(char shortOption, String longOption) {
            super(shortOption, longOption);
        }

        @Override
        public Object getArgument() {
            return null;
        }

        @Override
        void parseArgument(String stringRepresentation) {
        }

        @Override
        boolean hasArgument() {
            return false;
        }
    }
}
