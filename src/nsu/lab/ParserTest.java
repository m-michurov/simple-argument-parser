package nsu.lab;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    void parse() {
        Parser.StringArgument in = new Parser.StringArgument(
                'i',
                "in",
                null
        );
        Parser.StringArgument out = new Parser.StringArgument(
                'o',
                "out",
                "out.txt"
        );
        Parser.IntegerArgument n = new Parser.IntegerArgument(
                'n',
                "num-of-words",
                2
        );
        Parser.IntegerArgument m = new Parser.IntegerArgument(
                'm',
                "min-occurrences",
                2
        );
        Parser.Flag help = new Parser.Flag('h', "help");

        Parser parser = new Parser(in, out, m, n, help);
        String[] args = new String[]{"0", "-i=in.txt", "--min-occurrences=10", "--out", "buff.dat", "-h", "-n", "999"};

        parser.parse(args);

        assertTrue(parser.getOption('h').isFound());
        assertTrue(parser.getOption('i').isFound());
        assertEquals(parser.getOption("in").getArgument(), "in.txt");
        assertTrue(parser.getOption('o').isFound());
        assertEquals(parser.getOption("out").getArgument(), "buff.dat");
        assertTrue(parser.getOption("num-of-words").isFound());
        assertEquals(parser.getOption('n').getArgument(), 999);
        assertTrue(parser.getOption("min-occurrences").isFound());
        assertEquals(parser.getOption('m').getArgument(), 10);

        String[] badArgs1 = new String[]{"-k"};
        String[] badArgs2 = new String[]{"-n="};
        String[] badArgs3 = new String[]{"--out", "-h"};
        String[] badArgs4 = new String[]{"-m", "7.62 high velocity"};

        assertThrows(NoSuchElementException.class, () -> parser.parse(badArgs1));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs2));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs3));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs4));
    }

    @Test
    void getOption() {
        Parser.StringArgument in = new Parser.StringArgument(
                'i',
                "in",
                null
        );
        Parser.StringArgument out = new Parser.StringArgument(
                'o',
                "out",
                "out.txt"
        );
        Parser.IntegerArgument n = new Parser.IntegerArgument(
                'n',
                "num-of-words",
                2
        );
        Parser.IntegerArgument m = new Parser.IntegerArgument(
                'm',
                "min-occurrences",
                2
        );
        Parser.Flag help = new Parser.Flag('h', "help");

        Parser parser = new Parser(in, out, m, n, help);

        assertFalse(parser.getOption('h').isFound());
        assertFalse(parser.getOption('i').isFound());
        assertFalse(parser.getOption('o').isFound());
        assertFalse(parser.getOption("num-of-words").isFound());
        assertFalse(parser.getOption("min-occurrences").isFound());

        assertNull(parser.getOption("in").getArgument());
        assertEquals(parser.getOption("out").getArgument(), "out.txt");
        assertEquals(parser.getOption('n').getArgument(), 2);
        assertEquals(parser.getOption('m').getArgument(), 2);
    }

    @Test
    void getFreeArguments() {
        Parser parser = new Parser(
                new Parser.StringArgument('n', "n", null),
                new Parser.StringArgument('a', "aaa", "default")
        );

        String[] free = new String[]{"arg1", "-n=3", "--aaa", "arg2", "third-arg"};

        parser.parse(free);

        assertEquals(parser.getFreeArguments(), new LinkedList<>(Arrays.asList("arg1", "third-arg")));

        String[] noFree = new String[]{"-n", "value", "-a=value"};

        parser = new Parser(
                new Parser.StringArgument('n', "n", null),
                new Parser.StringArgument('a', "aaa", "default")
        );

        parser.parse(noFree);

        assertTrue(parser.getFreeArguments().isEmpty());
    }
}